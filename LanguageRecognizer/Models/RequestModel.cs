﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LanguageRecognizer.Models
{
    public class RequestModel
    {
        public int RequestId { get; set; }

        public int UserId { get; set; }

        public string Word { get; set; }

		public DateTime RequestTime { get; set; }
    }
}