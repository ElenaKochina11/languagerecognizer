﻿using LanguageRecognizer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LanguageRecognizer.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Register()
        {
            return View();
        }
		[HttpPost]
		public async Task<ActionResult> Register(RegisterModel model)
		{
			if (ModelState.IsValid)
			{
				User user = new User { UserName = model.UserName, Email = model.Email };
				//IdentityResult result = await UserManager.CreateAsync(user, model.Password);
				//if (result.Succeeded)
				//{
				//	return RedirectToAction("Login", "Account");
				//}
				//else
				//{
				//	foreach (string error in result.Errors)
				//	{
				//		ModelState.AddModelError("", error);
				//	}
				//}
			}
			return View(model);
		}
	}
}